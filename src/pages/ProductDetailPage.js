
import Navbar from "../features/Navbar/navbar";
import ProductDetail from "../features/products/ProductDetail/productDetail";
function ProductDetailPage() {
    return (<>
        <Navbar>
            <ProductDetail></ProductDetail>
        </Navbar>

    </>)
}

export default ProductDetailPage;