import React from "react";
import Navbar from "../features/Navbar/navbar";
import ProductList from "../features/products/ProductList/productList";
function Home() {
    return (<>
    
        <Navbar>
            <ProductList></ProductList>
        </Navbar>
  
    </>)
}

export default Home;