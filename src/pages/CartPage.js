import { Cart } from "../features/cart/Cart";
import Navbar from "../features/Navbar/navbar";
function CartPage() {
    return (<>
        <Navbar>
            <Cart />
        </Navbar>
    </>)
}

export default CartPage;