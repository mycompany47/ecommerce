import { configureStore } from '@reduxjs/toolkit';
import productReducer from "../../src/features/products/ProductList/ProductListSlice";

export const store = configureStore({
  reducer: {
    product: productReducer,
  },

});
console.log("store", store)